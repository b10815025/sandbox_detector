appmgrSchema = {
    "type": "object",
    "properties": {
        "data": {
            "type": "object",
            "properties": {
                "eventType": {"type": "string"},
                "maxRetries": {"type": "number"},
                "retryTimer": {"type": "number"},
                "targetUrl": {"type": "string",
                              "enum": ["http://service-ricplt-rtmgr-http:3800/ric/v1/handles/xapp-handle/", "http://service-ricplt-vespamgr-http.ricplt.svc.cluster.local:8080/ric/v1/xappnotif"]
                              }
            }
        }
    }
}

submgrSchema = {
    "properties": {
        "Valid": {"type": "boolean"},
        "ReqId": {
            "type": "object",
            "properties": {
                "Id": {"type": "number"},
                "InstanceId": {"type": "number"}
            }
        },
        "Meid": {
            "type": "object",
            "properties": {
                "PlmnId": {"type": "string"},
                "EnbID": {"type": "string"},
                "RanName": {"type": "string"}
            }
        },
        "EpList": {
            "type": "object",
            "properties": {
                "Endpoints": {
                    "type": "array",
                    "item": {
                        "type": "object",
                        "properties": {
                            "Addr": {"type": "string"},
                            "Port": {"type": "number"}
                        }
                    }
                }
            }
        },
        "SubReqMsg": {
            "type": "object",
            "properties": {
                "Id": {"type": "number"},
                "InstanceId": {"type": "number"},
                "FunctionId": {"type": "number"},
                "Data": {
                    "type": "object",
                    "properties": {
                        "Length": {"type": "number"},
                        "Data": {"type": "string"}
                    }
                },
                "ActionSetups": {
                    "type": "array",
                    "items": {
                        "type": "object",
                        "properties": {
                            "ActionId": {"type": "number"},
                            "ActionType": {"type": "number"},
                            "RicActionDefinitionPresent": {"type": "boolean"},
                            "Data": {
                                "type": "object",
                                "properties": {
                                    "Length": {"type": "number"},
                                    "Data": {"type": "null"}
                                }
                            },
                            "Present": {"type": "boolean"},
                            "Type": {"type": "number"},
                            "TimetoWait": {"type": "number"}
                        }
                    }
                }
            }
        },
        "SubRespMsg": {
            "type": "object",
            "properties": {
                "Id": {"type": "number"},
                "InstanceId": {"type": "number"},
                "FunctionId": {"type": "number"},
                "ActionAdmittedList": {
                    "type": "object",
                    "properties": {
                        "Items": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "ActionId": {"type": "number"}
                                }
                            }
                        }
                    }
                },
                "ActionNotAdmittedList": {
                    "type": "object",
                    "properties": {
                        "Items": {"type": "array"}
                    }
                }
            },
            "SubRespRcvd": {"type": "string"},
            "PolicyUpdate": {"type": "boolean"}
        }
    }
}

e2generalSchema = {
    "properties": {
        "enableRic": {"type": "boolean"}
    }
}

e2addrSchema = {
    "type": "string"
}

e2eNBSchema = {
    "type": "string"
}

e2InstanceSchema = {
    "properties": {
        "address": {"type": "string"},
        "podName": {"type": "string", "enum": ["e2term"]},
        "associatedRanList": {
            "type": "array",
            "items": {
                "type": "string"
            }
        },
        "keepAliveTimestamp": {"type": "number"},
        "state": {"type": "string"},
        "deletionTimeStamp": {"type": "number"}
    }
}

e2RanSchema = {
    "type": "string"
}

e2eNBAddrSchema = {
    "type": "string"
}

ueJsonSchema = {
    "type": "object",
    "properties": {
        "PDCP-Bytes-DL": {"type": "number"},
        "Neighbor-Cell-RF": {"type": "null"},
        "UE ID": {
            "type": "string",
            "minLength": 1,
            "maxLength": 20
        },
        "Meas-Time-RF": {
            "type": "object",
            "properties": {
                "tv_sec": {"type": "number"},
                "tv_nsec": {"type": "number"}
            }
        },
        "Meas-Timestamp-PDCP-Bytes": {
            "type": "object",
            "properties": {
                "tv_sec": {"type": "number"},
                "tv_nsec": {"type": "number"}
            }
        },
        "PRB-Usage-UL": {"type": "number"},
        "Serving-Cell-RF": {
            "type": "object",
            "properties": {
                "rsrq": {"type": "number"},
                "rsrp": {"type": "number"},
                "rsSinr": {"type": "number"}
            }
        },
        "PDCP-Bytes-UL": {"type": "number"},
        "Serving Cell ID": {
            "type": "string",
            "minLength": 1
        },
        "PRB-Usage-DL": {"type": "number"},
        "Meas-Timestamp-PRB": {
            "type": "object",
            "properties": {
                "tv_sec": {"type": "number"},
                "tv_nsec": {"type": "number"}
            }
        }
    }
}
